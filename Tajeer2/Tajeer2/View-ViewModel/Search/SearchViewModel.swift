//
//  SearchViewModel.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/2/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class SearchViewModel {
    
    var searchedItems = [Products]()
    var page = 1
    var type = "search"
    var didPaginationEnded = false
    var itemsCount : Int {
        return searchedItems.count
    }
    
    func getSearchedItems(searchedKey:String,success:@escaping()->Void,failure:@escaping(NSError)->Void) {
        DataSource.shared.searchForItem(type: type, page: page, searchq: searchedKey, success: { (items) in
            
                if (items.products?.count ?? 0) < 20 {
                    self.didPaginationEnded = true
                    
                }else {
                    
                }
            self.searchedItems.append(contentsOf: items.products ?? [])
            success()
        }, failure: failure)
    }
    
    func stopApiRequest() {
        DataSource.shared.stopApiRequest()
    }
}
