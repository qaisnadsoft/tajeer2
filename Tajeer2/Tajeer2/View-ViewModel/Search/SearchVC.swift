//
//  SearchVC.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/2/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Kingfisher
import IQKeyboardManagerSwift
import Toast_Swift

class SearchVC: UIViewController {
    deinit {
        print("No memory leaks for SearchVC")
    }
    //Outlets:-
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    //Variables:-
    private var isSearching = false
    var searchText = ""
    var style = ToastStyle()
    private let viewModel = SearchViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuerUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
     func configuerUI() {
        setupCollectionView()
        setupSearchBar()
        setupNavigation()
        style.titleFont = AppFonts.font(forWeight: .plain, size: 17)
    }
    
    @objc func configuerData() {
        
        _ = SKPreloadingView.show()
        if isSearching {
            viewModel.stopApiRequest()
            viewModel.searchedItems.removeAll()
        }
        
        viewModel.getSearchedItems(searchedKey: searchText, success: { [weak self] in
            guard let self = self else { return }
           
            SKPreloadingView.hide()
            if self.viewModel.itemsCount == 0 {
                self.view.makeToast("لا توجد نتائج بحث", duration: 2, position: .center)
            }
            self.itemsCollectionView.reloadData()
        }) { [weak self] (err) in
            guard let self = self else { return }
            SKPreloadingView.hide()
            self.view.makeToast(err.localizedDescription, position: .center)
        }
    }
    
    func setupCollectionView() {
        itemsCollectionView.register(ItemsCell.self)
        itemsCollectionView.dataSource = self
        itemsCollectionView.delegate = self
    }
    
    func setupSearchBar() {
        searchBar.delegate = self
        self.view.endEditing(false)
        searchBar.showsCancelButton = true
        searchBar.placeholder = "بحث..."
        searchBar.becomeFirstResponder()
        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            textField.font = AppFonts.font(forWeight: .plain, size: 17)
        }
    }
    
    func setupNavigation() {
        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
    }
}

extension SearchVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.searchedItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = itemsCollectionView.dequeueReusableCell(forIndexPath: indexPath) as ItemsCell
        
        let info = viewModel.searchedItems[indexPath.item]
        
        let url = URL(string:info.image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
        
        cell.itemImg.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
        cell.itemImg.contentMode = .scaleAspectFill
        cell.itemImg.kf.indicatorType = .activity
        cell.itemImg.backgroundColor = .black
        
        cell.itemDate.font = AppFonts.font(forWeight: .plain, size: 17)
        cell.itemDate.textColor = .white
        cell.itemDate.text = info.datetime.formattedDate
        
        cell.itemTitle.font = AppFonts.font(forWeight: .bold, size: 17)
        cell.itemTitle.textColor = .black
        cell.itemTitle.text = info.title
        
        cell.itemPrice.font = AppFonts.font(forWeight: .bold, size: 20)
        cell.itemPrice.textColor = UIColor(red: 161/255, green: 190/255, blue: 84/255, alpha: 1)
        cell.itemPrice.text = info.price
        
        cell.labelGradiantView.backgroundColor = .clear
        cell.labelGradiantView.applyGradient(colors: [.black,.clear])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width - 30) / 2
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: Routing.Storyboards.items.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.itemsDetailsVC.rawValue) as? ItemsDetailsVC {
            vc.viewModel.productID = viewModel.searchedItems[indexPath.item].productID
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
}

extension SearchVC:UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else { return }
        if text.count >= 3 && text.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.searchText = text
            self.isSearching = true
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(configuerData), object: nil)
            self.perform(#selector(configuerData), with: nil, afterDelay: 0.0)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        if text.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            self.view.makeToast("الرجاء ملئ الحقل" ,position:.center)
        } else {
            searchText = text
            self.isSearching = true
            configuerData()
            print("Searching...")
            self.view.endEditing(true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
}
