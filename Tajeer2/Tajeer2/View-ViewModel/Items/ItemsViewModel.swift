//
//  ItemsViewModel.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class itemsViewModel {
    
    
    var products = [Products]()
    var catID:String?
    var type = "cat"
    var productID:String?
    var productInfo:ProductDetails?
    var title = ""
    var page = 1
    var productCount:Int {
        return products.count
    }
    
    func getItems(success:@escaping()->Void,failure:@escaping(NSError)->Void) {
        guard let id = catID else {
            return
        }
        DataSource.shared.getItemsByCatID(type: type, catID: id, page: page, success: { (items) in
            
            if items.count != 0 {
                if let products = items[0].products {
                    self.products = products
                }
            }
            
            success()
        }, failure: failure)
    }
    
    func getItemsDetails(success:@escaping()->Void,failure:@escaping(NSError)->Void) {
        guard let id = productID else {
            return
        }
        DataSource.shared.getProductInfo(productID: id, success: { (productDetails) in
            self.productInfo = productDetails.product
            success()
        }, failure: failure)
    }
}
