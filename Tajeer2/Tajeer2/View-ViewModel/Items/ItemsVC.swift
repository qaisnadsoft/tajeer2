//
//  ItemsVC.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Kingfisher

class ItemsVC: UIViewController {
    deinit {
        print("No memory leaks for Items VC")
    }
    //Outlets:-
    
    @IBOutlet weak var itemsCollectionView: UICollectionView!
    //Variables:-
    
    let viewModel = itemsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuerUI()
        configuerData()
    }
    
    func configuerUI() {
        setupCollectionView()
        setupNavigation()
    }
    
    func configuerData() {
        _ = SKPreloadingView.show()
        
        viewModel.getItems(success: { [weak self] in
            
            guard let self = self else {
                return
            }

            self.itemsCollectionView.reloadData()
            SKPreloadingView.hide()
        }) { [weak self] (err) in
            
            guard let self = self else {
                return
            }
            
            SKPreloadingView.hide()
            self.view.makeToast(err.localizedDescription)
        }
    }
    
    func setupNavigation() {
        
        let lbl = UILabel()
        lbl.text = viewModel.title
        lbl.font = AppFonts.font(forWeight: .bold, size: 20)
        self.navigationItem.titleView = lbl
        
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchAction))
        self.navigationItem.rightBarButtonItem = searchBtn
        
        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
        
        navigationController?.navigationBar.barTintColor = UIColor.lightBlue

    }

    
    @objc func searchAction (_ sender:UIButton) {
        let vc = UIStoryboard(name: Routing.Storyboards.search.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.searchVC.rawValue)
        
        let navigationVC = UINavigationController.init(rootViewController: vc)
        self.navigationController?.present(navigationVC, animated: true, completion: nil)

    }
    
    func setupCollectionView() {
        itemsCollectionView.register(ItemsCell.self)
        itemsCollectionView.delegate = self
        itemsCollectionView.dataSource = self
    }
}

extension ItemsVC:UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.productCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = itemsCollectionView.dequeueReusableCell(forIndexPath: indexPath) as ItemsCell
        
        let info = viewModel.products[indexPath.item]
        
        let url = URL(string:info.image.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
        
        cell.itemImg.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
        cell.itemImg.contentMode = .scaleAspectFill
        cell.itemImg.kf.indicatorType = .activity
        cell.itemImg.backgroundColor = .black
        
        cell.itemDate.font = AppFonts.font(forWeight: .plain, size: 17)
        cell.itemDate.textColor = .white
        cell.itemDate.text = info.datetime.formattedDate
        
        cell.itemTitle.font = AppFonts.font(forWeight: .bold, size: 17)
        cell.itemTitle.textColor = .black
        cell.itemTitle.text = info.title
        
        cell.itemPrice.font = AppFonts.font(forWeight: .bold, size: 20)
        cell.itemPrice.textColor = UIColor(red: 161/255, green: 190/255, blue: 84/255, alpha: 1)
        cell.itemPrice.text = info.price
        
        cell.labelGradiantView.backgroundColor = .clear
        cell.labelGradiantView.applyGradient(colors: [.black,.clear])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width - 30) / 2
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: Routing.Storyboards.items.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.itemsDetailsVC.rawValue) as? ItemsDetailsVC {
            viewModel.productID = viewModel.products[indexPath.item].productID
            vc.viewModel = self.viewModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
