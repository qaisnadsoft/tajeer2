//
//  ItemsDetailsVC.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit


class ItemsDetailsVC: UIViewController {
    deinit {
        print("No memory leaks for ItemsDetailsVC")
    }
    //Outlets:-
    @IBOutlet weak var itemTableView: UITableView!
    
    //Variables:-
    var viewModel = itemsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configuerUI()
        configuerData()
    }
    
    func configuerUI() {
        setupTableView()
        setupNavigation()
        
    }
    
    func configuerData() {
        _  = SKPreloadingView.show()
        viewModel.getItemsDetails(success: { [weak self] in
            guard let self = self else {
                return
            }
            SKPreloadingView.hide()
            self.itemTableView.reloadData()
        }) { [weak self] (err) in
            guard let self = self else {
                return
            }
            
            SKPreloadingView.hide()
            self.view.makeToast(err.localizedDescription)
        }
    }
    
    func setupTableView() {
        itemTableView.register(ItemHeaderCell.self)
        itemTableView.register(ItemBodyCell.self)
        itemTableView.dataSource = self
        itemTableView.delegate = self
    }
    
    func setupNavigation() {
        
        let imgView = UIImageView()
        imgView.image = UIImage(named: "logosmall-1")
        imgView.contentMode = .scaleAspectFit
        self.navigationItem.titleView =  imgView
        
        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
        
        navigationController?.navigationBar.barTintColor = UIColor.lightBlue
    }
    
    @objc func callDealer(_ sender:UIButton) {
        let phoneNumber = viewModel.productInfo?.phone ?? ""
        guard let url = URL(string: "tel://" + phoneNumber) else { return }
        UIApplication.shared.open(url)
    }
    
    @objc func sharePost(_ sender:UIButton){
        
        let url =  ApiEndPoints.baseUrl + ApiEndPoints.shareProduct.rawValue + (viewModel.productID ?? "")
        let items = [url]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
        
    }
}

extension ItemsDetailsVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = itemTableView.dequeueReusableCell(forIndexPath: indexPath) as ItemHeaderCell
            cell.navigation = self.navigationController ?? UINavigationController()
            cell.viewModel = viewModel
            cell.imgsCollectionView.reloadData()
            
            cell.shareBtn.addTarget(self, action: #selector(sharePost), for: .touchUpInside)
            cell.callBtn.addTarget(self, action: #selector(callDealer), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = itemTableView.dequeueReusableCell(forIndexPath: indexPath) as ItemBodyCell
            cell.selectionStyle = .none
            let info = viewModel.productInfo
            
            cell.cellTitle.font = AppFonts.font(forWeight: .bold, size: 25)
            cell.cellTitle.text = info?.title
            cell.cellTitle.numberOfLines = 2
            
            cell.date.font = AppFonts.font(forWeight: .plain, size: 17)
            cell.date.text = info?.datetime.formattedDate
            
            cell.location.font = AppFonts.font(forWeight: .plain, size: 17)
            cell.location.text = info?.address
            
            cell.body.font = AppFonts.font(forWeight: .plain, size: 17)
            cell.body.text = info?.productBody
            cell.body.textColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 1)
            cell.body.numberOfLines = 0
            
            if let url = URL(string: info?.ads.adLink ?? "") {
                let urlRequest = URLRequest(url: url)
                cell.webView.load(urlRequest)
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return tableView.bounds.size.height / 2.5
        }
        
        return UITableView.automaticDimension
        
    }
}
