//
//  MainVC.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Kingfisher


class MainVC: UIViewController {
    deinit {
        print("No memory leaks for MainVC")
    }
    //Outlets:-
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    //Variables:-
    private let viewModel = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuerUI()
        configuerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    func configuerUI() {
        setupCollectionView()
        setupNavigation()
        
    }
    
    func configuerData() {
        _ = SKPreloadingView.show()
        viewModel.getCategories(success: { [weak self] in
            guard let self = self else {
                return
            }
            
            SKPreloadingView.hide()
            self.categoriesCollectionView.reloadData()
        }) { [weak self] (err) in
            guard let self = self else {
                return
            }
            SKPreloadingView.hide()
            self.view.makeToast(err.localizedDescription)
        }
        
    }
    
    func setupCollectionView() {
        categoriesCollectionView.register(MainCatCell.self)
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
    }
    
    func setupNavigation() {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "logosmall-1")
        imgView.contentMode = .scaleAspectFit
        self.navigationItem.titleView =  imgView
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchAction))
        self.navigationItem.rightBarButtonItem = searchBtn
        
        let addItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItem(_:)))
        
        self.navigationItem.leftBarButtonItem = addItem
        
        let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBtn
        
        navigationController?.navigationBar.barTintColor = UIColor.lightBlue

        
    }
    
    @objc func addItem(_ sender:UIButton) {
        let endPoint = ApiEndPoints.baseUrl
        guard let url = URL(string: endPoint) else { return }
        UIApplication.shared.open(url)
    }

    @objc func searchAction(_ sender:UIButton){
        
        let vc = UIStoryboard(name: Routing.Storyboards.search.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.searchVC.rawValue)
        let navigationVC = UINavigationController.init(rootViewController: vc)
        self.navigationController?.present(navigationVC, animated: true, completion: nil)
        
    }
}

extension MainVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.categoriesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCollectionView.dequeueReusableCell(forIndexPath: indexPath) as MainCatCell
        let info = viewModel.categories[indexPath.item]
        
        let url = URL(string: info.picture.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
        let placeHolderImg = UIImage(named: "placeHolder")
        cell.cellImg.kf.setImage(with: url,placeholder: placeHolderImg)
        cell.cellImg.contentMode = .scaleAspectFill
        cell.cellImg.kf.indicatorType = .activity
        cell.cellImg.backgroundColor = .black
        
        cell.cellTitle.text = info.catName
        cell.cellTitle.font = AppFonts.font(forWeight: .bold, size: 20)
        cell.cellTitle.textColor = .white
        cell.cellTitle.numberOfLines = 2
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width:CGFloat = 0
        let height:CGFloat = (collectionView.bounds.size.width - 10) / 2
        if indexPath.item % 3 == 0 {
            width = collectionView.bounds.size.width - 20
        } else {
            width = (collectionView.bounds.size.width - 30 ) / 2
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: Routing.Storyboards.items.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.itemsVC.rawValue) as? ItemsVC {
            let info = viewModel.categories[indexPath.item]
            vc.viewModel.catID = info.catID
            vc.viewModel.title = info.catName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

