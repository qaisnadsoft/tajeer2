//
//  MainViewModel.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class MainViewModel {
    
    var categories = [CategoriesModel]()
    
    var categoriesCount:Int{
        return categories.count
    }
    
    func getCategories(success:@escaping()->Void,failure:@escaping(NSError)->Void) {
        DataSource.shared.getMainCategories(type: "main", success: { (categories) in
            self.categories = categories
            success()
        }, failure: failure)
    }
}
