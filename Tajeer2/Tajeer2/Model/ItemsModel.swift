//
//  ItemsModel.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/1/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class ItemsModel:Codable {
    
    var catName:String?
    var catID:String?
    var catDesign:String?
    var products : [Products]?
    
    enum CodingKeys:String,CodingKey {
        case catName = "CatName"
        case catID = "CatID"
        case catDesign = "CatDesign"
        case products = "Products"
    }
}

class Products:Codable {
    
    var productID:String
    var datetime:String
    var title:String
    var price:String
    var image:String
    var intro:String
    var category:String
    var externalLink:String
    var type:String?
    var typeID:String?
    
    enum CodingKeys:String,CodingKey {
        case productID = "ProductID"
        case datetime = "Datetime"
        case title = "Title"
        case price = "Price"
        case image = "Image"
        case intro = "Intro"
        case category = "Category"
        case externalLink = "ExternalLink"
        case type = "Type"
        case typeID = "TypeID"
    }
}

class ProductInfo:Codable {
    var product:ProductDetails
    
    private enum CodingKeys:String,CodingKey{
        case product = "Product"
    }
}

class ProductDetails: Codable {
    var images:[String]
    var ads:AdsInfo
    var datetime:String
    var price:String
    var title:String
    var productID:String
    var address:String
    var name:String
    var phone:String
    var phone2:String
    var email:String?
    var productBody:String
    
    private enum CodingKeys:String,CodingKey{
       case images = "Images"
       case ads = "Ads"
       case datetime = "Datetime"
       case price = "Price"
       case title = "Title"
       case productID = "ProductID"
       case address = "Address"
       case name = "Name"
       case phone = "Phone"
       case phone2 = "Phone2"
       case email = "Email"
       case productBody = "ProductBody"
    }
}

class AdsInfo: Codable {
    var adLink:String
    var adHideAfter:String
    
    private enum CodingKeys:String,CodingKey {
        case adLink = "AdLink"
        case adHideAfter = "AdHideAfter"
    }
}


