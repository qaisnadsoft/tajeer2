//
//  CategoriesModel.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/1/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

class CategoriesModel:Codable {
    
    var catName:String
    var catID:String
    var picture:String
    
    enum CodingKeys:String,CodingKey {
        case catName = "CatName"
        case catID = "CatID"
        case picture = "Picture"
    }
}
