//
//  SplashScreenVC.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class SplashScreenVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2)  { [weak self] in
            
            guard let self = self else {
                return
            }
            
            let vc = UIStoryboard(name: Routing.Storyboards.main.rawValue , bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.mainVC.rawValue)
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    

}
