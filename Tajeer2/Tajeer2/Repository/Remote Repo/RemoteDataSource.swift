//
//  RemoteDataSource.swift
//  BaseProject
//
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation
import Alamofire

class RemoteDataSource : DataSourceProtocol {
    
    //MARK:-Proparties
    lazy var remoteContext = RemoteContext(baseURL: ApiEndPoints.baseUrl)
    
    func getMainCategories(type: String, success: (([CategoriesModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getMain.rawValue
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "mod":type
        ]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode([CategoriesModel].self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }

        }
    }
    
    
    func getItemsByCatID(type: String, catID: String,page:Int, success: (([ItemsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getMain.rawValue
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "mod":type,
            "CatID":catID,
            "page":page
            ] as [String : Any]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode([ItemsModel].self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }

        }
    }
    
    func getProductInfo(productID: String, success: ((ProductInfo) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getProductInfo.rawValue
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "ID":productID
        ]
        
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode(ProductInfo.self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }
        }
    }
    
    func searchForItem(type: String, page: Int, searchq: String, success: ((ItemsModel) -> Void)?, failure: ((NSError) -> Void)?) {
        
        let url = ApiEndPoints.getMain.rawValue.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        let endPoint = EndPoint(address: url, httpMethod: .get)
        let prams = [
            "searchq":searchq,
            "mod" : type,
            "page":page
            
            ] as [String : Any]
        remoteContext.request(endPoint: endPoint, parameters: prams) { (result, data) in
            
            guard result else {
                //Failure
                let error = data as? NSError
                print("Error occured while authenticating user. In \(#function). \(error!.localizedDescription)")
                failure?(error!)
                return
            }
            
            let decoder = JSONDecoder()
            
            do{
                let model = try decoder.decode(ItemsModel.self, from: data as! Data)
                success?(model)
            }catch let err{
                failure?(err as NSError)
            }
        }
    }
    
    
    func stopApiRequest() {
        remoteContext.stopRequests()
    }
}
    
    

