
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

final class DataSource : DataSourceProtocol {
    
    static var shared: DataSourceProtocol = DataSource()
    
    lazy var remoteDataSource:DataSourceProtocol = RemoteDataSource()

    
    func getMainCategories(type: String, success: (([CategoriesModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getMainCategories(type: type, success: { (categories) in
            success?(categories)
        }, failure: failure)
    }
    
    func getItemsByCatID(type: String, catID: String,page:Int, success: (([ItemsModel]) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getItemsByCatID(type: type, catID: catID, page: page, success: { (items) in
            success?(items)
        }, failure: failure)
    }
    
    func getProductInfo(productID: String, success: ((ProductInfo) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.getProductInfo(productID: productID, success: { (itemDetails) in
            success?(itemDetails)
        }, failure: failure)
    }
    
    func searchForItem(type: String, page: Int, searchq: String, success: ((ItemsModel) -> Void)?, failure: ((NSError) -> Void)?) {
        remoteDataSource.searchForItem(type: type, page: page, searchq: searchq, success: { (items) in
            success?(items)
        }, failure: failure)
    }
    
    func stopApiRequest() {
        remoteDataSource.stopApiRequest()
    }
}
