
//  Created by Qais Alnammari on 2/11/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

protocol DataSourceProtocol {
    
    
    func getMainCategories(type:String,success:(([CategoriesModel])->Void)? , failure:((NSError)->Void)?)
    
    func getItemsByCatID(type:String,catID:String,page:Int,success:(([ItemsModel])->Void)? , failure:((NSError)->Void)?)
    func getProductInfo(productID:String,success:((ProductInfo)->Void)? , failure:((NSError)->Void)?)
    func searchForItem(type:String,page:Int,searchq:String,success:((ItemsModel)->Void)? , failure:((NSError)->Void)?)
    
    func stopApiRequest()
}
