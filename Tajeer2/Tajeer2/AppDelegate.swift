//
//  AppDelegate.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AlamofireNetworkActivityLogger
import Toast_Swift
import OneSignal
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        NetworkActivityLogger.shared.startLogging()
        NetworkActivityLogger.shared.level = .debug
        
        AppLanguageManager.shared.changeLanguage(lang: .ar  )
        
        var style = ToastStyle()
        style.messageFont = AppFonts.font(forWeight: .plain, size: 17)
        ToastManager.shared.style = style
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        let font = AppFonts.font(forWeight: .bold, size: 17)
        UINavigationBar.appearance().tintColor = .black
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font : font,NSAttributedString.Key.foregroundColor : UIColor.black], for: .normal)
        UIBarButtonItem.appearance().tintColor = UIColor.black
        
        
        //Notification
      
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        

        OneSignal.initWithLaunchOptions(launchOptions, appId: "73e5d725-5022-4dd0-aa7e-90beef73061c", handleNotificationAction: { (result) in
            if let dic = result?.notification.payload.additionalData {
                let mainStoryboard : UIStoryboard = UIStoryboard(name: Routing.Storyboards.items.rawValue, bundle: nil)

                if let vc = mainStoryboard.instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.itemsDetailsVC.rawValue) as? ItemsDetailsVC {
                    vc.viewModel.productID = (dic["productID"] as? String) ?? ""
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    let navigation = UINavigationController(rootViewController: vc)
                    navigation.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.backToMainMenu(_:)))
                    self.window?.rootViewController = navigation
                    self.window?.makeKeyAndVisible()
                }
                
                
            }
            
        }, settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted, error) in
            if granted {
                OneSignal.promptForPushNotifications(userResponse: { accepted in
                    print("User accepted notifications: \(accepted)")
                })
            }
        }
        
        
        return true
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        //this method will be called when a url open the app
        guard let url = userActivity.webpageURL else { return false }
        
        if let vc = UIStoryboard(name: Routing.Storyboards.items.rawValue, bundle: nil).instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.itemsDetailsVC.rawValue) as? ItemsDetailsVC {
            let components = url.absoluteString.split(separator: "=")
            guard let id = components.last else {return false}
            vc.viewModel.productID = "\(id)"
            self.window?.rootViewController = UINavigationController(rootViewController: vc)
            self.window?.makeKeyAndVisible()
            
        }
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    @objc func backToMainMenu(_ sender:UIButton) {
        print("Success")
        let mainStoryboard : UIStoryboard = UIStoryboard(name: Routing.Storyboards.main.rawValue, bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: Routing.VCsStoryboardsIds.mainVC.rawValue)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let navigation = UINavigationController(rootViewController: vc)
            self.window?.rootViewController = navigation
            self.window?.makeKeyAndVisible()
    }

}

