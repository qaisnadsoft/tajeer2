
//  Created by Qais Alnammari on 6/23/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation

struct Routing {
    enum Storyboards:String {
        case main = "Main"
        case items = "Items"
        case search = "Search"
    }
    
    enum VCsStoryboardsIds:String {
        case SplashScreenVC = "SplashScreenVC"
        case mainVC = "MainVC"
        case itemsVC = "ItemsVC"
        case itemsDetailsVC = "ItemsDetailsVC"
        case searchVC = "SearchVC"
    }
}
