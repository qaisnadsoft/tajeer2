//
//  MainCatCell.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class MainCatCell: UICollectionViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellImg.layer.cornerRadius = 5
        
    }

}
