//
//  ItemImgCell.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class ItemImgCell: UICollectionViewCell {

    @IBOutlet weak var cellImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellImg.backgroundColor = .black
    }

}
