//
//  ItemsCell.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

class ItemsCell: UICollectionViewCell {

    @IBOutlet weak var labelGradiantView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mainView.layer.cornerRadius = 5
        
    }
    
}
