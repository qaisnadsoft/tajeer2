//
//  ItemBodyCell.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/1/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import WebKit
class ItemBodyCell: UITableViewCell {

    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var cellTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
