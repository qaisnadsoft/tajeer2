//
//  ItemHeaderCell.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit
import Lightbox
import Kingfisher

class ItemHeaderCell: UITableViewCell {

    @IBOutlet weak var tracerView: UIView!
    @IBOutlet weak var imgCountIndecator: UILabel!
    @IBOutlet weak var imgsCollectionView: UICollectionView!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    var navigation = UINavigationController()
    var viewModel = itemsViewModel()
    var tracerCount = 1
    var imgCount = 0
    var lastContentOffset:CGFloat = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupCollectionView()
        tracerView.applyGradient(colors: [.clear,.black]) //startPoint: .topLeft, endPoint: .bottomRight)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) { [weak self] in
            guard let self = self else { return }
            
            if let count = self.viewModel.productInfo?.images.count {
                self.imgCount = count
                if count == 0 {
                    self.tracerCount = 0
                }
                self.imgCountIndecator.text = "\(self.tracerCount)/\(self.imgCount)"
            }
        }
        
    }
    
    func setupCollectionView() {
        imgsCollectionView.register(ItemImgCell.self)
        imgsCollectionView.delegate = self
        imgsCollectionView.dataSource = self
    }
}

extension ItemHeaderCell:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = viewModel.productInfo?.images.count ?? 0
        if count != 0 {
            return count
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let count = viewModel.productInfo?.images.count ?? 0
        let cell = imgsCollectionView.dequeueReusableCell(forIndexPath: indexPath) as ItemImgCell
        if count != 0 {
            
            let info = viewModel.productInfo
            
            let url = URL(string:info?.images[indexPath.item].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "" )
            
            cell.cellImg.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
            cell.cellImg.kf.indicatorType = .activity
            
            return cell
        } else {
            
            cell.cellImg.image = UIImage(named: "placeHolder")
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.size.width
        let height = collectionView.bounds.size.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let count = viewModel.productInfo?.images.count ?? 0
        if count != 0 {
        var lightImgs = [LightboxImage]()
        
        guard let imgURLs = viewModel.productInfo?.images else {
            return
        }
        
        for imgURL in imgURLs {
            let url = URL(string: imgURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")

            let imgView = UIImageView()
            imgView.kf.setImage(with: url, placeholder: UIImage(named: "placeHolder"))
            if let unrappedImg = imgView.image {
                lightImgs.append(LightboxImage(image: unrappedImg))
            }
        }

        let controller = LightboxController(images: lightImgs, startIndex: indexPath.item)
        navigation.present(controller,animated:true)
        } else {
            self.makeToast("لا توجد صور",position:.center)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.x
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset  < scrollView.contentOffset.x ) {
            if lastContentOffset.truncatingRemainder(dividingBy: imgsCollectionView.bounds.size.width)  == 0 {
                tracerCount -= 1
            }
             // swiped right

        } else if (self.lastContentOffset  > scrollView.contentOffset.x) {
            if lastContentOffset.truncatingRemainder(dividingBy: imgsCollectionView.bounds.size.width)  == 0 {
                tracerCount += 1
            }
            // swiped left

        }
         imgCountIndecator.text = "\(tracerCount)/\(imgCount)"
    }
}
