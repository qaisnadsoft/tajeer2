//
//  String+Ext.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/2/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation


extension String {
    
//    static func setDateFormatter(date:String)->String {
    var formattedDate:String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy/MM/dd"
        let date = formatter.date(from: self)
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: date ?? Date())
        
    }
}
