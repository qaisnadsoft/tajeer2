//
//  UIColor+Ext.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 7/1/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var lightBlue:UIColor {
        return UIColor(red: 244/255, green: 251/255, blue: 255/255, alpha: 1)
    }
}
