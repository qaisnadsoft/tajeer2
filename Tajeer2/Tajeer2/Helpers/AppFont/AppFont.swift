//
//  AppFont.swift
//  Tajeer2
//
//  Created by Qais Alnammari on 6/30/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation
import UIKit

class AppFonts {
    
    enum Weight {
        case plain
        case bold
        
        var familyName:String {
            switch self {
            case .plain:
                return "Al-Jazeera-Arabic-Regular"
            case .bold:
                return "Al-Jazeera-Arabic-Bold"
            }
        }
    }
    
    class func font(forWeight weight:Weight,size:CGFloat) -> UIFont {
        guard let localizedFont = UIFont(name: weight.familyName, size: size) else {
            print("Error : Couldnt find the custom font")
            return UIFont.systemFont(ofSize: size)
        }
        return localizedFont
    }
}
