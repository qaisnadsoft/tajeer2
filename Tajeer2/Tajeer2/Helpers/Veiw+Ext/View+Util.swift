//
//  View+Util.swift
//  BaseProject
//
//  Created by Qais Alnammari on 6/23/19.
//  Copyright © 2019 Qais Alnammari. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

extension UIView {
    
//    public func makeToast(_ message: String) {
//        var style = ToastManager.shared.style
//        style.messageAlignment = .natural
//        style.messageFont = AppFonts.font(forWeight: .regular, size: 15)
//        self.makeToast(message, style: style)
//    }
    
    func makeCircular() {
        let side = min(bounds.size.width, bounds.size.height)
        layer.cornerRadius = side/2
    }
    
    func addCorner(radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    
    func subviewsRecursive() -> [UIView] {
        return subviews + subviews.flatMap { $0.subviewsRecursive() }
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    //
    func addBorder(color: UIColor, width: CGFloat) {
        layer.masksToBounds = false
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func copyView() -> UIView? {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as? UIView
    }
    
    func animate(borderColor: UIColor) {
        let colorAnimation = CABasicAnimation(keyPath: "borderColor")
        colorAnimation.fromValue = layer.borderColor
        colorAnimation.toValue = borderColor.cgColor
        colorAnimation.duration = 0.2
        colorAnimation.isRemovedOnCompletion = false
        colorAnimation.fillMode = .forwards
        layer.add(colorAnimation, forKey: "borderColor")
    }
    func circularCornerRadius(viewController:UIViewController){
        let proportion:CGFloat
        proportion = self.frame.size.height/viewController.view.frame.size.height
        self.layer.cornerRadius = (UIScreen.main.bounds.height*proportion)/2
    }
    
    func circularCornerRadiusStoryboard(viewController:UIViewController , totalView:UIView){
        var proportion:CGFloat
        proportion = (self.frame.size.height/totalView.frame.size.height)*0.93
        self.layer.cornerRadius = (UIScreen.main.bounds.height*proportion)/2
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y:self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
    
    
    func changContentAnimation(duration:CFTimeInterval)  {
        let animation: CATransition = CATransition()
        animation.duration = duration
        self.layer.add(animation, forKey:  "L")
    }
    
    func selectionAnimation()  {
        self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.2) {
            self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
        UIView.animate(withDuration: 0.1, delay: 0.2, options:.allowAnimatedContent, animations: {
            self.transform = .identity
        }, completion: nil)
    }
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable
    var storyboard: String?  {
        get {
            return Holder.storyboardValue
        }
        set {
            if let color = newValue {
                Holder.storyboardValue = color
            } else {
                Holder.storyboardValue = nil
            }
        }
    }
    
    struct Holder {
        static var storyboardValue:String? = ""
    }
}



class GradientLayerView: UIView {
    static let tag = 52347632
    
    var colors: [UIColor] = [.white]
    var startPoint: CAGradientLayer.Point = .topLeft
    var endPoint: CAGradientLayer.Point = .bottomLeft
    var corner: CGFloat = 0
    var gradientLayer: CAGradientLayer!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    init(frame: CGRect, colors: [UIColor], startPoint: CAGradientLayer.Point = .topLeft, endPoint: CAGradientLayer.Point = .bottomLeft, corner: CGFloat) {
        super.init(frame: frame)
        
        self.colors = colors
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.corner = corner
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradientLayer.frame = self.bounds
    }
    private func setupView() {
        self.backgroundColor = .clear
        self.isUserInteractionEnabled = false
        self.tag = tag
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        gradientLayer = CAGradientLayer(frame: self.bounds, colors: colors, startPoint: startPoint, endPoint: endPoint)
        
        gradientLayer.cornerRadius = corner
        self.layer.addSublayer(gradientLayer)
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
}

extension UIView{
    func applyGradient(colors: [UIColor], startPoint: CAGradientLayer.Point = .topLeft, endPoint: CAGradientLayer.Point = .bottomLeft) {
        removeGradient()
        
        let gradientView = GradientLayerView(frame: self.bounds, colors: colors, startPoint: startPoint, endPoint: endPoint, corner: self.layer.cornerRadius)
        
        self.insertSubview(gradientView, at: 0)
    }
    
    func removeGradient(){
        for view in self.subviews{
            if let gview = view as? GradientLayerView{
                gview.removeFromSuperview()
            }
        }
        //        self.viewWithTag(GradientLayerView.tag)?.removeFromSuperview()
    }
}
